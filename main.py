from pocketsphinx import *
import pyaudio
import os 
curr_dir = os.path.dirname(os.path.realpath(__file__))

pocketsphinx_dir = os.path.dirname(pocketsphinx.__file__)
pocketsphinx_model = os.path.join(pocketsphinx_dir, 'model')
model_dir = os.path.join(curr_dir, 'model')
model_dir_lang = os.path.join(model_dir, 'en-us')

config = pocketsphinx.Decoder.default_config()
config.set_string('-hmm', os.path.join(pocketsphinx_model, 'en-us'))
config.set_string('-keyphrase', 'WINIDO')
config.set_string('-dict', os.path.join(model_dir_lang, "9060.dic"))
config.set_float('-kws_threshold', 1e-20)
decoder = Decoder(config)

p = pyaudio.PyAudio()

stream = p.open(format=pyaudio.paInt16, channels=1, rate=16000, input=True, frames_per_buffer=20480)
stream.start_stream()
decoder.start_utt()
while(True):
	buf = stream.read(1024)
	if buf:
			decoder.process_raw(buf,False,False)
	else:
		break;

	if decoder.hyp() is not None:
		print("Hotword Detected")
		decoder.end_utt()
		print("Procee here")
		decoder.start_utt()